# ![Moto_Boot_Logo_Maker](https://raw.githubusercontent.com/Franco28/Moto_Boot_Logo_Maker/master/Logo/BootLogoMaker.jpg)


# Getting Started (Read all please!)

## First of all, im not responsible of any device damage, you are accepting a license when you install the Tool and you are using this at your own risk!

### Moto Boot Logo Maker for Motorola Moto Devices (4MiB, 6MiB, 8MiB, 16MiB & 32MiB) 

### Support only Qualcomm devices not MediaTek

### Support 10+ languages! (English (United States), Spanish (Mexico), Turkish, German (Germany), Portuguese (Brazil), Japanese, Polish, Russian, Italian (Italy), French (France), Irish

#

#### * [Downloads](https://github.com/Franco28/Moto_Boot_Logo_Maker#downloads) 
#### * [Contact](https://github.com/Franco28/Moto_Boot_Logo_Maker#contact) 

## Prerequisites Windows
- You must have NetFramework 4+ installed on your PC * [Net Framework](https://dotnet.microsoft.com/download) 
- Windows 7+ x64 (64) or x86 (32) Bits

## Install
- Start the "Moto_Boot_Logo_Maker_x.x.x_Setup.exe"
- The installer will create a desktop shortcut and in Windows app will create a folder with the tool inside. 

## How it works
- The installation will create a "Moto_Boot_Logo_Maker" shortcut on desktop
- When you launch the Tool on the first time this will:

-- unpack on "C:\Users\%USERNAME%\AppData\Local\Franco28\ -> (Tool Settings)
-- unpack on "C:\adb\" -> (ADB&FASTBOOT)
-- unpack on "C:\NewMotoLogo\" --> future builds of logos, you can change this!

-- Then you are free to load any logo and change logo images!


####

#

## Screenshots


### Main Tool
![Main Tool](https://raw.githubusercontent.com/Franco28/Moto_Boot_Logo_Maker/master/Logo/ToolLicense.png "Main Tool")

### Settigs Tool
![Settigs Tool](https://raw.githubusercontent.com/Franco28/Moto_Boot_Logo_Maker/master/Logo/settings.png "Settigs Tool")

### Test your logo
![Test your logo](https://raw.githubusercontent.com/Franco28/Moto_Boot_Logo_Maker/master/Logo/testurlogo.png "Test your logo")

### Search Device
![Search Device](https://raw.githubusercontent.com/Franco28/Moto_Boot_Logo_Maker/master/Logo/SearchDevice.png "Search Device")

### About Tool 
![About Tool](https://raw.githubusercontent.com/Franco28/Moto_Boot_Logo_Maker/master/Logo/about.png "About Tool")


####

#

## Contact 
#### [Telegram](https://t.me/francom28) 


## Credits
#### Thanks to [CaitSith2](https://github.com/CaitSith2/MotoBootLogoMaker) main Tool creator!
#### Thanks to [regaw-leinad](https://github.com/regaw-leinad/AndroidLib) for this Android lib!
#### Thanks to [rbsoft](https://github.com/ravibpatel/AutoUpdater.NET) for this Update lib!
